# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import discord
import requests
from flask import request
import flask
import bank

class VerificationServer():
	"""A component that opens an http server for verifying Discord accounts, and
	linking them to user's IP addresses.

	This proccess involves sending the user through the Discord oauth proccess,
	meaning that the user start's on Discord's "sign in with Discord" page, and is
	redirected here.  The application then redeems the oauth code that the user is
	sent with for a bearer token, that is used as authentication when asking for the
	user's Discord profile, of which the user's ID and name are taken and stored
	along with the user's ip address and an initial balance of 0 credits.

	Attributes:
		app (flask.Flask): The flask app representing the server.  Run app.run() to
			start the webserver, once the VerificationServer has been initialized.
		bank (bank.Bank): The bank in which new users are to be registered
		client_id (str): The Client ID issued by Discord to your app.  This and the
			client secret should probably be loaded from a config file, and should be
			unique to the instance.
		client_secret (str): The Client Secret issued to your app by Discord.  Used
			in addition to the client_id to complete the oauth process.  Keep this
			very safe.
		redirect (str):	The redirect url that is registered with your app in Discord
		on_register (list[function]): A list of event listeners to be called
			when a new user is registered.  Each function should except a
			Discord ID as a string.
	
	Args:
		linking_bank (bank.Bank): See the `bank` attribute
		oauth_client_id (str): See the `client_id` attribute
		oauth_client_secret (str): See the `client_secret` attribute
		oauth_redirect (str): See the `redirect` attribute
	"""
	
	def __init__(self, linking_bank, oauth_client_id, oauth_client_secret, oauth_redirect):
		self.app = flask.Flask(__name__)
		self.bank = linking_bank
		self.client_id = oauth_client_id
		self.client_secret = oauth_client_secret
		self.redirect = oauth_redirect
		self.app.add_url_rule('/','process_request',self.process_request)
		self.on_register = []
	
	def process_request(self):
		"""The handler for new requests to the server.  Behaves in accordance with a
		function flagged with the route decorator in a Flask app.
		"""
		if 'code' in request.args:
			error = self.pair_by_oauth(request.args.get('code'), request.access_route[0])
			if error == 0:
				return "Success"
			elif error == 1:
				return "Invalid authorization code.  Try logging in again"
			elif error == 2:
				return "You've already registered!"
			else:
				return "Unknown error!"
		return "Please login with Discord to proceed"
	
	def pair_by_oauth(self, oauth, ip):
		"""Adds a new user to the database based on an OAuth code and ip

		Args:
			oauth (str): The OAuth code that the user was sent to the page with
			ip (str): The user's ip address
		Returns:
			int: 0 if the process was successfull, 1 if an invalid code was
				used, 2 if the user already exists
		"""
		print(f'link {oauth} - {ip}')
		resp = requests.post('https://discordapp.com/api/v6/oauth2/token', data={
			'client_id': self.client_id,
			'client_secret': self.client_secret,
			'grant_type': 'authorization_code',
			'code': oauth,
			'redirect_uri': self.redirect,
			'scope': 'identify'
		})
		if 'error' in resp.json():
			return 1
		token = resp.json()['access_token']
		resp = requests.get('https://discordapp.com/api/v6/users/@me', headers = {
			'Authorization': f'Bearer {token}'
		})
		user_data = resp.json()
		success = self.bank.register_user(user_data['id'], ip, user_data['username'])
		if success:
			for function in self.on_register:
				function(user_data['id'])
		return 0 if success else 2
	
	@property
	def oauth_url(self):
		"""str:  The url users should be sent to in order to use the verification server.
		"""
		return f"https://discordapp.com/api/oauth2/authorize?client_id={self.client_id}&redirect_uri={self.redirect}&response_type=code&scope=identify"
