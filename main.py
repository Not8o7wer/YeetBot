# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import discord
import yaml
import threading
import bank
import verifier
import bot
import shop
import sqlalchemy
import asyncio

def run(config_file='config.yml'):
	config = None
	with open(config_file, 'r') as config_data:
		config = yaml.load(config_data)
	config['bot']['channels'] = [str(chan) for chan in config['bot']['channels']]
	config['bot']['admins'] = [str(chan) for chan in config['bot']['admins']]
	oauth = config['oauth']
	database = sqlalchemy.create_engine(config.get('database', 'sqlite:///database.db'))
	bnk = bank.Bank(engine=database)
	for cpm in config['payment_methods']['continuous']:
		bnk.cpms.add(bank.ContinuousPaymentModule.from_module_name(cpm, config['modules'][cpm], database))
	shp = shop.Shop(config['shop'],engine=database)
	verserv = verifier.VerificationServer(bnk, oauth['client_id'], oauth['client_secret'], oauth['redirect_url'])
	client = bot.YeetBot(bnk,shp,verserv.oauth_url,config['modules'],config['bot'])

	def greet(discord_id):
		user = discord.User()
		user.id = discord_id
		client.loop.create_task(client.send_message(user, content='Signup successfull!  Welcome aboard!'))
	verserv.on_register.append(greet)

	def run_bot(): client.run(oauth['bot_token'])
	def run_web(): verserv.app.run(host=config['web']['host'], port=config['web']['port'])

	verserv_thread = threading.Thread(target=run_web)
	bot_thread = threading.Thread(target=run_bot)
	verserv_thread.start()
	bot_thread.start()

if __name__ == '__main__':
	run()
