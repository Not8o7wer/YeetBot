discord.py==0.16.12
PyYAML==3.12
SQLAlchemy==1.2.8
Flask==0.12.2
requests==2.18.4
